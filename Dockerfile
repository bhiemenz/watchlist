###
# Project: watchlist
# Description: 
###
FROM composer:1.5
LABEL maintainer="mail@bhiemenz.net"

ENV DEBIAN_FRONTEND noninteractive
ENV USER watcher
ENV PROJECT_PATH /home/${USER}/watchlist 

RUN  apk update && \
     apk add \
       bash \
       autoconf \
       g++ \
       openssl-dev \
       make && \
     rm -rf /var/cache/apk/*
        
# add user
RUN adduser -D -g "" ${USER}

# add project files and install 
COPY    ./   ${PROJECT_PATH} 
RUN     chown -R 1000:1000 ${PROJECT_PATH}
USER    ${USER}
RUN     cd ${PROJECT_PATH} && composer install     

EXPOSE 8080
WORKDIR ${PROJECT_PATH}/src
CMD ["php", "-S", "0.0.0.0:8080"]
