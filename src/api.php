<?php
class API {

    const IMDB_ID_PATTERN = "/^tt[0-9]+$/";

    private $finder;
    private $database;
    private $logger;

    public function __construct($finder, $database, $logger) {
        $this->finder = $finder;
        $this->database = $database;
        $this->logger = $logger;
    }

    public function deleteMovie($id) {

        // check id
        if(!(preg_match(self::IMDB_ID_PATTERN, $id))) {
            return array('status' => array('code' => 400, 'message' => 'Bad Request', 'reason' => 'Not a valid id'));
        }
        $this->database->delete($id);
        $this->logger->debug("Deleted movie", ['data' => $movie]);
        return array('status' => array('code' => 204, 'message' => 'No Content'));
    }

    public function getMovie($id) {

        // check id
        if(!(preg_match(self::IMDB_ID_PATTERN, $id))) {
            return array('status' => array('code' => 400, 'message' => 'Bad Request', 'reason' => 'Not a valid id'));
        }

        // get movie and check result from database
        $movie = $this->database->get($id);
        if (empty($movie)) {
            return array('status' => array('code' => 404, 'message' => 'Not Found', 'reason' => 'Id could not be found in the database'));
        }
        
        $this->logger->debug("Get movie", ['data' => $movie]);  
        return array('status' => array('code' => 200, 'message' => 'OK'), 'data' => $movie);
    }

    public function getMovies() {
        $movies = $this->database->getAll();
        
        $this->logger->debug("Get all movies", $movies);  
        return array('status' => array('code' => 200, 'message' => 'OK'), 'data' => $movies);
    }

    public function addMovie($id) {

        // check id
        if (!(preg_match(self::IMDB_ID_PATTERN, $id))) {
            return array('status' => array('code' => 400, 'message' => 'Bad Request', 'reason' => 'Not a valid id'));
        }

        try {
            // find information about movie
            $movie = $this->finder->findMovie($id);
        } catch (Exception $e) {
            return array('status' => array('code' => 500, 'message' => 'Internal Server Error', 'reason' => 'API key is not valid'));
        } 

        if (empty($movie['title'])) {
            return array('status' => array('code' => 404, 'message' => 'Not Found', 'reason' => 'Could not found movie'));
        }
                
        // save movie to database
        $result = $this->database->insert($movie);
        $this->logger->debug("Inserted new movie with data", $movie);
              
        return array('status' => array('code' => 200, 'message' => 'OK'), 'data' => $movie);
    }
}
