<?php

require '../vendor/autoload.php';

// prepare settings
$settings = require __DIR__ . '/settings.php';

// create application
$app = new \Slim\App($settings);

// get container
$container = $app->getContainer();

// create view
$container['view'] = function ($container) {
    $view = new \Slim\Views\Twig('../views/', [
        'cache' => false
    ]);

    // Instantiate and add Slim specific extension
    $router = $container->get('router');
    $uri = \Slim\Http\Uri::createFromEnvironment(new \Slim\Http\Environment($_SERVER));
    $view->addExtension(new Slim\Views\TwigExtension($router, $uri));

    return $view;
};

// create logger
$container['logger'] = function($c) {
    $logger = new \Monolog\Logger('watchlist_logger');
    $file_handler = new \Monolog\Handler\StreamHandler("../logs/app.log", Monolog\Logger::DEBUG);
    $logger->pushHandler($file_handler);
    return $logger;
};

// create database
$container['database'] = function ($c) {
    $db = $c['settings']['db'];
    
    if ($db['type'] === 'json') {
        $dir = '../database/';
        $database = new FileSystem($dir, $c['logger']);
    }
    
    $database->init();
    return $database;
};

// create movie finder
$container['finder'] = function ($c) {
    $finder = $c['settings']['finder'];
    
    $name = $finder['name']; // not required so far...
    $apiKey = $finder['apiKey'];
       
    return FinderFactory::create($apiKey);
};

// other files
require __DIR__ . '/api.php';
require __DIR__ . '/api_routes.php';
require __DIR__ . '/gui_routes.php';
require __DIR__ . '/database/database.php';
require __DIR__ . '/database/filesystem.php';
require __DIR__ . '/finder/finder.php';
require __DIR__ . '/finder/moviedb.php';

// run application
$app->run();
